A simple image gallery for ecommerce sites based on the ebay model. 
Designed to be modular the max with is 500px. Just grab the html starting with
gallery div. Add the images you'd like 
to the "gallery-thumbs-inner" div. Square images work best. 

This is responsive. It removes the main image div and makes the lower
thumbnail row into a scrolling div with all the images.

Colors can easily be changed in with the css variables at the top
of the css file. The controls for the thumbnails are svg that can b
changed in Inkscape.
