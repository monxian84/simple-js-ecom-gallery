//Get main image and thumbs divs swap out the image src
// on mouseover
var gallery_main_img = document.getElementById('main-img');
var getThumbs = document.getElementById("gallery-thumbs-inner");


//Put the first thumb into the main image div
var url = new URL(getThumbs.firstElementChild.src);
gallery_main_img.src = url.pathname;

  
//Add event listeners to thumbs for on mouseover change the main image
getThumbs.addEventListener("mouseover", function(e){
  if(e.target.nodeName == 'IMG'){
    var thumbHovered = (e.target.attributes[0].value);
    gallery_main_img.src = thumbHovered;
    }
});

//Count the thumbs and make calculate the size of thumbs container
//subtract the visible portion of the container 415px in this case.
var thumbCount = getThumbs.childElementCount;
var containerWidth = (thumbCount * 75)-415;


//start margin at zero movemargin the size of the image
var margin = 0;
var moveMargin = 83;

//get the left and right arrow buttons
var leftCtl = document.getElementById('control-left');
var rightCtl = document.getElementById('control-right');


//move the margin left don't go past 0
leftCtl.addEventListener('click', function(){
    if(margin > 0 ){
        margin = margin - moveMargin;
        getThumbs.style.marginLeft = "-" + margin +"px";
    }
});

//move the margin right don't go past the container max width 
//which was calculated above
rightCtl.addEventListener('click', function(){
    if(margin < containerWidth){
        margin = margin + moveMargin;
        getThumbs.style.marginLeft = "-" + margin +"px";
    }
});